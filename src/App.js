import React from 'react';
import './App.css';
import CityName from './components/CityName';
import DisplayWeather from './components/DisplayWeather';
import DisplayMap from './components/DisplayMap';

let city = 'oslo';
const API_KEY = '9721686f3d39f5b254428b074f1412bf';
const URL = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`;

// Component to get the city name.
// Component to get and display weather.
// Component to display a map.

const App = () => {
  return (
    <div className="App">
    
      <CityName city="olso" />
      <DisplayWeather city="oslo" API_KEY = "9721686f3d39f5b254428b074f1412bf"/>
      <DisplayMap coordinates />
    </div>
  );
}

export default App;
