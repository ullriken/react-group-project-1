import React, { useEffect, useState } from 'react';




const DisplayWeather = (props) => {

    const { city, API_KEY } = props;

    const [humidity, setHumidity] = useState({});
    const [temerature, setTemerature] = useState({});
    const [windSpeed, setWindSpeed] = useState({});
    const [weatherDescription, setweatherDescription] = useState({});
    const [coordinates, setCoordinates] = useState({});

    useEffect(() => {

        fetch(`https://api.openweathermap.org/data/2.5/weather?appid=${API_KEY}&units=metric&q=${city}`)
        .then(resp => resp.json())
        .then(response => {
            // response = weather data
            setHumidity(response.main.humidity);
        
            setTemerature(response.main.temp);
            setWindSpeed(response.wind.speed);
            setweatherDescription(response.weather[0].description);
            setCoordinates(response.coord);
            
            // main.himidity
            // main.temp
            // wind.speed
            // weather.description
            
        });

    }, [city, API_KEY])

    return (
        coordinates,
        <div>
            <h1>Display the Weather</h1>
            <h4>Humidity: {JSON.stringify(humidity)} </h4>
            <h4>Temerature: {JSON.stringify(temerature)} </h4>
            <h4>Windspeed: {JSON.stringify(windSpeed)} </h4>
            <h4>Weather Description: {JSON.stringify(weatherDescription)} </h4>
        </div>
    );
}

export default DisplayWeather;