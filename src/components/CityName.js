import React from 'react';

const CityName = (props)=> {
    const { city } = props;
    return (
        <div>
            <h1>City Name</h1>
            <h4>{city}</h4>
        </div>
    );
}

export default CityName;